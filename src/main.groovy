import groovy.xml.XmlParser
import groovy.xml.XmlUtil

def processMuleXmlFiles(File xmlFile) {
    def globalXmlParser = new XmlParser().parse(xmlFile)
    if (globalXmlParser.name().localPart != 'mule') return

    for (jmsNode in globalXmlParser.'jms:connector') {
        def activeMqNode = jmsNode.clone()
        activeMqNode.name().localPart = "activemq-connector"
        activeMqNode.attributes().put("specification", "1.1")
        activeMqNode.attributes().put("brokerURL", "ssl://b-40e9cf0c-718e-4dde-8335-dc3723d54e0e-2.mq.us-east-2.amazonaws.com:61617")
        List<Node> toRemoveNode = activeMqNode.value().stream()
                .filter(n -> n.name().localPart == "property")
                .collect(java.util.stream.Collectors.toList())
        activeMqNode.value().removeAll(toRemoveNode)
        jmsNode.replaceNode(activeMqNode)
    }
    println XmlUtil.serialize(globalXmlParser)
}

def isPomFileContainspropertiestoberemoved(line) {
    def propertiesList = ['com.tibco.tibjms.connect.attempts',
                          'com.tibco.tibjms.connect.attempt.timeout',
                          'com.tibco.tibjms.reconnect.attempts',
                          'com.tibco.tibjms.reconnect.attempt.timeout']
    for (it in propertiesList) {
        if (line.contains(it))
            return true
    }
    return false
}

def addNewDependencies(dependency) {
    def newdependencydetails = [
            [
                    groupId   : "org.apache.activemq",
                    artifactId: "activemq-client",
                    version   : "5.15.0"
            ],
            [
                    groupId   : "org.apache.activemq",
                    artifactId: "activemq-pool",
                    version   : "5.15.0"
            ]
    ]
    newdependencydetails.each { prop ->
        def childNodeList = []
        def newDependency = dependency.clone()
        for (Node child : newDependency.value()) {
            childNodeList.add(new Node(null, child.name(), prop.get(child.name().localPart)))
        }
        dependency.parent().append(new Node(null, newDependency.name(), childNodeList))
        println(dependency)
    }

}

def processPomXmlFiles(File xmlFile) {
    File tempFile = File.createTempFile("temp", ".tmp")
    def dependencies2Remove

    xmlFile.withReader { reader ->
        while ((line = reader.readLine()) != null) {
            if (!isPomFileContainspropertiestoberemoved(line))
                tempFile.append(line)
        }
    }

    def pomXmlParser = new XmlParser().parse(tempFile)
    if (pomXmlParser.name().localPart != 'project') return

    pomXmlParser.'dependencies'.getAt('dependency').each {
        if (['javax.jms', 'com.tibco'].contains(it.get('groupId').text())) {
            if (dependencies2Remove == null)
                dependencies2Remove = new Node(it.parent(), it.name(), it.value())
            it.parent().remove(it)
        }
    }
    addNewDependencies(dependencies2Remove)

    println XmlUtil.serialize(pomXmlParser)
}


new File(".").eachDirRecurse() { dir ->
    dir.eachFileMatch(~/.*.xml/) { file ->
        if (file.name.endsWith("pom.xml")) processPomXmlFiles(file)
        else processMuleXmlFiles(file)
    }
}


