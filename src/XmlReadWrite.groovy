import groovy.io.FileType
import groovy.namespace.QName
import groovy.xml.XmlParser
import groovy.xml.XmlUtil


class XmlReadWrite {

    static Properties properties = new Properties()

    def static loadProperties() {

        File propertiesFile = new File('src/ActiveMQtags.properties')
        propertiesFile.withInputStream {
            properties.load(it)
        }
    }

    def static processMuleXmlFiles(File xmlFile) {
        def globalXmlParser = new XmlParser().parse(xmlFile)
        if (!(globalXmlParser.name() instanceof QName)
                || globalXmlParser.name().localPart != properties.muleRootNode)
            return

        for (jmsNode in globalXmlParser.'jms:connector') {
            def newNode = jmsNode.clone()
            newNode.name().localPart = "activemq-connector"

            //Add new Attribute specified in addProperties
            for (String attrToAdd : properties.addActiveMqProperties.split(',')) {
                newNode.attributes().put(attrToAdd, properties.getAt(attrToAdd))
            }
            // Remove attribute
            for (String attrToRemove : properties.removeJmsTag.split(',')) {
                newNode.attributes().remove(attrToRemove)
            }

            // Remove the Child element specifiend in properties replaceType
            List<Node> toRemoveNode = newNode.value().stream()
                    .filter({ n -> n.name().localPart == properties.removeExistingJmsChildNode })
                    .collect(java.util.stream.Collectors.toList())
            newNode.value().removeAll(toRemoveNode)
            //Replace the existing Node with New Node
            jmsNode.replaceNode(newNode)
            println 'proccessing completed in ' + xmlFile.getPath()
        }
        // println XmlUtil.serialize(globalXmlParser)
        xmlFile.withWriter('UTF-8') { writer ->
            writer.write(XmlUtil.serialize(globalXmlParser))
        }
    }

    def static processPomXmlFiles(File xmlFile) {
        File tempFile = File.createTempFile("temp", ".tmp")

        try {
            removedUnwantedProfileProperty(xmlFile, tempFile)
            def pomXmlParser = new XmlParser().parse(tempFile)
            if (!(pomXmlParser.name() instanceof QName)
                    || pomXmlParser.name().localPart != properties.pomXmlRootNode)
                return
            addNewDependencies(removePomDependencies(pomXmlParser))
            xmlFile.withWriter('UTF-8') { writer ->
                writer.write(XmlUtil.serialize(pomXmlParser))
            }
        } catch (Exception ex) {
            println("Exception occurred in processing pom file. path : " + xmlFile.path)
            println('Exception : ' + ex.printStackTrace())
        } finally {
            tempFile.delete()
        }
    }

    private static Object removePomDependencies(Node pomXmlParser) {
        def dependencies2Remove = null
        pomXmlParser.'dependencies'.getAt('dependency').each {
            if (properties.removePomDependenciesWithArtifactId.split(',').contains(it.get('artifactId').text())) {
                if (dependencies2Remove == null)
                    dependencies2Remove = new Node(it.parent(), it.name(), it.value())
                it.parent().remove(it)
            }
        }
        return dependencies2Remove
    }

    private static Object removedUnwantedProfileProperty(File xmlFile, tempFile) {
        String line = ""
        xmlFile.withReader { reader ->
            while ((line = reader.readLine()) != null) {
                if (!isPomFileContainsPropertiesToBeRemoved(line))
                    tempFile.append(line)
            }
        }
    }

    def static isPomFileContainsPropertiesToBeRemoved(line) {
        for (String property2remove : properties.pomProfilePropertiesToRemove.split(',')) {
            if (line.contains(property2remove))
                return true
        }
        return false
    }

    def static addNewDependencies(sampleDependency) {
        1.upto(properties.noOfNewDependenciesToAdd.toInteger(), { seq ->
            properties."newPomDependency.${seq}".split(',').each { dependency ->
                String[] array = dependency.split(':')
                def prop = [groupId: array[0], artifactId: array[1], version: array[2]]
                def childNodeList = []
                def newDependency = sampleDependency.clone()

                for (Node child : newDependency.value()) {
                    childNodeList.add(new Node(null, child.name(), prop.get(child.name().localPart)))
                }
                sampleDependency.parent().append(new Node(null, newDependency.name(), childNodeList))
                println(dependency)
            }
        })
        sampleDependency.parent().remove(sampleDependency)
    }


    static void main(String... args) {
        // Intitalize the Properties file
        loadProperties();

        println("Enter the root directory");
        def rootDirectory = new Scanner(System.in).nextLine();

        def list = []

        def dir = new File(rootDirectory)
        dir.eachFileRecurse(FileType.FILES) { file ->
            list << file
        }

        list.each {
            println("File under process: " + it.path)
            if (it.path.endsWith("pom.xml")) {
                processPomXmlFiles(it)
            } else if (it.path.endsWith(".xml") && !it.path.endsWith("log4j2.xml")) {
                processMuleXmlFiles(it);
            }
        }
    }

}